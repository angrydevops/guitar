<?php
/**
 * Created by PhpStorm.
 */

namespace vBulletin\Search;

/**
 * Простейший рендер
 * Class View
 * @package vBulletin\Search
 */
class View
{
    /**
     * @param $file
     * @param array $data
     * @return string
     */
    function render($file, $data = []): string
    {
        extract($data);

        ob_start();
        include __DIR__ . "/views/" . $file;
        $renderedView = ob_get_clean();

        echo $renderedView;
    }
}