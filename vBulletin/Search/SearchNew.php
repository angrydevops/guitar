<?php
/**
 * Created by PhpStorm.
 */

namespace vBulletin\Search;


class SearchNew
{
    /**
     * не известно какой тип возращется, void использовать не уместно
     * @return null|string
     */
    public static function doSearch(): ?string
    {
        /**
         * Т.к. похоже что поисковый метод использует в основом GET параметры, то логичнее использовать $_GET
         * Так же для безопасности нужно объявить все необходимые параметры через переменные и обезопасить их от инъекций
         */
        [htmlspecialchars($searchId), htmlspecialchars($do), htmlspecialchars($q)] = $_GET;

        $view = new View();

        switch ($do){
            case 'showresults' && null !== $searchId:
                /**
                 * разнесем логику рендера, так удобно управлять частями кода
                 */
                return self::showResults($searchId);
                break;
            case 'process':
                return self::showProcess($q);
                break;
            default:
                return self::renderSearchResult();
                break;
        }
    }

    /**
     * @param int $searchId
     * @return null|string
     */
    private static function showResults(int $searchId): string
    {
        /**
         * Подключаемся к БД, можно вынести в отдельный сервис
         */
        $db = DB::getInstance();
        $mysqli = $db->getConnection();

        $query = $mysqli->prepare('SELECT * FROM vb_searchresult WHERE searchid = :searchid AND forum_id !=5');
        $query->bindValue(':searchid', $searchId, \PDO::PARAM_INT);

        $result = $query->execute();

        return self::renderSearchResult($result);
    }

    private static function showProcess(string $search): string
    {
        /**
         * Подключаемся к БД, можно вынести в отдельный сервис
         */
        $db = DB::getInstance();
        $mysqli = $db->getConnection();

        $query = $mysqli->prepare('SELECT * FROM vb_post WHERE  text like :query AND forum_id !=5');
        $query->bindValue(':query', $search, \PDO::PARAM_STR);

        $result = $query->execute();

        return self::renderSearchResult($result);
    }

    /**
     * используем camelCase для методов
     * @param $result
     * @return string
     */
      private static function renderSearchResult($result = null): string
    {
        /**
         *  Выносим рендер в отдельный метод
         */
        $view = new View();
        if(null !== $result) {
            return $view->render('result.php', ['data' => $result]);
        }

        return $view->render('default.php');
    }
}