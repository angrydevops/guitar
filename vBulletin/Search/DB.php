<?php
/**
 * Created by PhpStorm.
 */

namespace vBulletin\Search;

/**
 * Одиночка подключения к БД
 * Class DB
 * @package vBulletin\Search
 */
class DB
{
    private $connection;
    private static $_instance;

    private $dbHost = "127.0.0.1";
    private $dbUser = "forum";
    private $dbPass = "123456";
    private $dbName = "vbforum";

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * DB constructor.
     */
    private function __construct()
    {
        try {
            $this->connection = new \PDO('mysql:host=' . $this->dbHost . ';dbname=' . $this->dbName, $this->dbUser, $this->dbPass);
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch (\PDOException $e) {
            die("Нет соединения с БД " . $e->getMessage());
        }
    }

    private function __clone()
    {
    }

    public function getConnection()
    {
        return $this->connection;
    }
}