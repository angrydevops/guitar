<?php
/**
 * Created by PhpStorm.
 * User: zimovid
 * Date: 16.08.18
 * Time: 23:29
 */

namespace app\helper;


class CommentsHelper
{

    public static function sort($input, $parentId = '')
    {
        $output = [];
        foreach ($input as $key => $item) {
            if ($item['parent_id'] == $parentId) {
                $item['children'] = self::sort($input, $item['id']);
                $output[] = $item;
            }
        }
        return $output;
    }
}