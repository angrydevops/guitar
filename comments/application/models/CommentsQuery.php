<?php

namespace app\models;

use app\helper\CommentsHelper;

/**
 * This is the ActiveQuery class for [[Comments]].
 *
 * @see Comments
 */
class CommentsQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[status]]='. Comments::STATUS_ACTIVE);
    }

    /**
     * {@inheritdoc}
     * @return Comments[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Comments|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
