<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

?>
<div class="post">
    <?= $delimiter ?? '' ?>
    <strong>Комментарий# <?= Html::encode($model['id']) ?></strong>
    <?= HtmlPurifier::process($model['comment']) ?>
    <a href="<?= \yii\helpers\Url::to(['site/delete' , 'id'=>$model['id']])?>"> Удалить</a>
    <strong>Автор: </strong>
    <?= HtmlPurifier::process($model['created_by'])?>
    <?php if (isset($model['children'])) {
        foreach ($model['children'] as $child) {
            $delimiter = isset($delimiter) ? $delimiter . '&nbsp;' : '&nbsp;';
            echo $this->render('_item', ['model' => $child, 'delimiter' => $delimiter]);
        }
    }
    ?>
</div>