<?php
/**
 * Created by PhpStorm.
 * User: zimovid
 * Date: 15.08.18
 * Time: 4:18
 */

namespace app\commands;


use app\models\Comments;
use app\models\User;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Expression;
use yii\helpers\Console;
use yii\helpers\VarDumper;

/**
 * Генерация пользователей
 * Class SeedController
 * @package app\commands
 */
class SeedController extends Controller
{
    public $defaultAction = 'add-users';

    /**
     * Генерация пользователей
     * @return int Exit code
     * @throws \yii\base\Exception
     */
    public function actionAddUsers()
    {
        $faker = \Faker\Factory::create();

        for ($i=0; $i <=20; $i++){
            $user = new User();
            $user->username = $faker->userName;
            $user->auth_key = \Yii::$app->security->generateRandomString();
            $user->password_hash = \Yii::$app->security->generatePasswordHash('123456');
            $user->email = $faker->email;
            if(!$user->save()){
                $this->stdout("Пользователь не создан. Проблема ".VarDumper::dumpAsString($user->firstErrors)." \n", Console::FG_RED);
                return ExitCode::UNSPECIFIED_ERROR;
            };
            $this->stdout("Пользователь создан \n", Console::FG_GREEN);
        }

        return ExitCode::OK;
    }

    /**
     * Генерация комментариев
     * @return int Exit codex
     */
    public function actionAddComments()
    {
        $users = User::find()->all();
        $faker = \Faker\Factory::create();

        foreach ($users as $user)
        {
            $comment = new Comments();
            $comment->comment = $faker->text;
            $comment->status = Comments::STATUS_ACTIVE;
            $comment->created_by = $comment->updated_by = $user->id;
            if(!$comment->save()){
                $this->stdout("Комментарий не создан. Проблема ".VarDumper::dumpAsString($comment->firstErrors)." \n", Console::FG_RED);
                return ExitCode::UNSPECIFIED_ERROR;
            };

            $this->stdout("Комментарий создан \n", Console::FG_GREEN);
        }

        return ExitCode::OK;
    }

    /**
     * Генерация суб комментариев
     * @return int Exit codex
     */
    public function actionAddSubComments()
    {
        $faker = \Faker\Factory::create();
        $comments = Comments::find()->all();

        for ($i= 0; $i<4; $i++) {
            $user = User::find()->orderBy(new Expression('rand()'))->one();
            foreach ($comments as $singleComment)
            {
                $comment = new Comments();
                $comment->parent_id = $singleComment->id;
                $comment->comment = $faker->text;
                $comment->status = Comments::STATUS_ACTIVE;
                $comment->created_by = $comment->updated_by = $user->id;
                if(!$comment->save()){
                    $this->stdout("Комментарий не создан. Проблема ".VarDumper::dumpAsString($comment->firstErrors)." \n", Console::FG_RED);
                    return ExitCode::UNSPECIFIED_ERROR;
                };

                $this->stdout("Комментарий создан \n", Console::FG_GREEN);
            }
        }

        return ExitCode::OK;
    }
}