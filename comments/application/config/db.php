<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=mariadb;dbname=guitar',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
