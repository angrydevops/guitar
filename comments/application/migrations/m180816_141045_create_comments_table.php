<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comments`.
 */
class m180816_141045_create_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ], $tableOptions);

        $this->createTable('{{%comments}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->comment('id родительского комментария')->null(),
            'comment' => $this->text(),
            'status' =>$this->integer(),
            'created_by' => $this->integer()->comment('кем создан'),
            'updated_by' => $this->integer()->comment('кем обновлен'),
            'created_at' => $this->timestamp()->comment('время создания'),
            'updated_at' => $this->timestamp()->comment('время обновления')
        ],$tableOptions);

        $this->createIndex('idx-comments-parent_id', '{{%comments}}', 'parent_id');
        $this->addForeignKey(
            'fk-comments-parent_id',
            '{{%comments}}',
            'parent_id',
            '{{%comments}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx-comments-created_by', '{{%comments}}', 'created_by');
        $this->addForeignKey(
            'fk-comments-created_by',
            'comments',
            'created_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
        $this->createIndex('idx-comments-updated_by', 'comments', 'updated_by');
        $this->addForeignKey(
            'fk-comments-updated_by',
            '{{%comments}}',
            'updated_by',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-comments-parent_id','{{%comments}}');
        $this->dropForeignKey('fk-comments-created_by','{{%comments}}');
        $this->dropForeignKey('fk-comments-updated_by','{{%comments}}');

        $this->dropTable('{{%user}}');

        $this->dropIndex('idx-comments-parent_id', '{{%comments}}');
        $this->dropIndex('idx-comments-created_by', '{{%comments}}');
        $this->dropIndex('idx-comments-updated_by', '{{%comments}}');

        $this->dropTable('{{%comments}}');
    }
}
