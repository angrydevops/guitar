<?php

use yii\db\Migration;

/**
 * Class m180816_183140_create_sort_procedure
 */
class m180816_183140_create_sort_procedure extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
        CREATE  PROCEDURE sort_comments()
        BEGIN
             START TRANSACTION;
               WITH RECURSIVE recurs AS
                (
                 SELECT *, 0 AS lev 
                 FROM comments WHERE parent_id IS NULL
                 UNION ALL
                 SELECT c.*, lev+1 
                 FROM comments AS c JOIN recurs AS r
                 ON r.id = c.parent_id
                )
                SELECT
                 re.*
                FROM recurs AS re 
                ORDER BY COALESCE(parent_id, id), parent_id IS NOT NULL, id;
             COMMIT;
        END;
        ");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("DROP PROCEDURE `sort_comments`");
    }
}
